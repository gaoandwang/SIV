import React, { Component } from 'react';
import { List } from 'antd-mobile';
import bottom from '../images/bottom.png'
export default class Member extends Component {
    render() {
      return (
        <div> 
          <div className="top">
            <span>测试公司</span>
            <img src={bottom} alt="下箭头"/>
          </div>
          <List className="my-list">
              <List.Item>content 1</List.Item>
              <List.Item>content 2</List.Item>
              <List.Item>content 3</List.Item>
          </List>
          Member
        </div>
      );
    }
  }