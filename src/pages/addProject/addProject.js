import React, { Component } from 'react';
import './addProject.less';
export default class AddProject extends Component {
    render() {
      return (
        <div className="add-container"> 
          <h2>新建项目</h2>
          <div className="wrap">
            <textarea placeholder="请输入项目地址"></textarea>
            <span className="explain">*以地址作为项目唯一名称，仅创建者后期可以修改</span>
          </div>
          <div className="wrap">
            <input type="text" defaultValue="详细版全包/半包项目模板"/>
            <span className="explain">说明：在'公司设置'处可新增、修改项目进度模板</span>
          </div>
          <button>确认</button>
        </div>
      );
    }
  }