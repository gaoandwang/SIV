import React, { Component } from 'react';
import { hashHistory } from 'react-router'
import axios from 'axios'
const znzLogo = require('../../images/logo.jpg')
export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state={
            show:true,
            count:60,     //计算60s
            tel:'',       //手机号码初始值
            code:'',
            timer:null
        }
    }
    handlegetTelVal=(e)=>{    //获取输入的手机号码
        this.setState({
            tel:e.target.value
        })
    }
    handlegetYZMVal=(e)=>{    //获取输入的验证码
        this.setState({
            code:e.target.value
        })
    }
    handlegetYZM=()=>{          //获取验证码事件
        let RegExp =/^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$/; //手机号码的正则
        if(RegExp.test(this.state.tel)){  //手机号码的判断为true
            this.refs.info1.style.display="none";
            this.refs.info2.style.display="block";
        var that=this;
        this.setState({
            timer:setInterval(()=>{  //秒数进行倒计时
                that.setState({
                    count:--this.state.count
                })
                if(this.state.count<=0){
                    this.refs.info1.style.display="block";
                    this.refs.info2.style.display="none";
                    clearInterval(this.state.timer)
                    this.setState({
                        count:60,
                    })
                }
            },1000)
        })
        axios({     //验证码的数据请求
            method:"GET",
            headers:{'Content-type':'application/json'},
            url:'http://app.zhuangneizhu.com/user/gainCode.do',
            params:{
                mobile:this.state.tel,
                version:"3.0",
                appType:"znz"
            }
        })
        .then(function (res) {
            console.log(res);
        })
        .catch(function (error) {
            console.log(error);
        });
        }else{
           console.log(123)
        }
    }
    handleLogin=()=>{
        axios({     //登陆的接口
            method:"GET",
            headers:{'Content-type':'application/json'},
            url:'http://app.zhuangneizhu.com/user/login.do',
            params:{
                mobile:this.state.tel,
                version:"3.0",
                companyName:"",
                code:this.state.code,
                type:"android",
            }
        })
        .then(function (res) {
            window.localStorage.organizationId=res.data.data.organizations[0].organizationId;
            window.localStorage.userId=res.data.data.userId;
            // message.organizationId=res.data.data.organizations[0].organizationId; //暂时这么写
            // message.userId=res.data.data.userId;
            console.log(res.data.data)
            if(res.data.code===10000){
                hashHistory.push('/project')
            }else if(res.data.code===10020){
                console.log('验证码有误')
            }else{
                console.log("其他原因")
            }
        })
        .catch(function (error) {
            console.log(error);
        });
    }
  render() {
    const styles={
        container:{display:'flex',flexDirection:'column',alignItems:'center',justifyContent:'center'},
        znzLogo:{padding:'1.5rem 0 0.72rem 0',width:'100%',display:'flex',justifyContent:'center'},
        logoImg:{height:'3.08rem'},
        form:{background:'#f5f8ff',width:'6.9rem',height:'1.76rem',borderRadius:'0.08rem',},
        input:{fontSize:'0.28rem',background:'#f5f8ff',width:'6.5rem',height:'0.88rem'},
        bInput:{borderBottom:'1px solid #E6E7EC'},
        inputCenter:{display:'flex',justifyContent:'center'},
        yzm:{position:'relative'},
        info:{position:'absolute',right:'0.20rem',top:'0.3rem',color:'#5677fc',fontSize:'0.28rem'},
        info1:{display:'block'},
        info2:{display:'none'},
        sure:{width:'6.9rem',height:'0.88rem',borderRadius:'0.08rem',marginTop:'0.40rem',marginBottom:'1.60rem',background:'#5677FC',color:'#fff',fontSize:'0.28rem'},
        btn:{width:'4.64rem',height:'0.64rem',display:'flex',flexDirection:'row',justifyContent:'space-between',margin:'0 auto'},
        button:{ border:'1px solid #5677FC',height:'0.64rem',fontSize:'0.28rem',background:'transparent',borderRadius:'0.32rem',color:'#5677fc'},
        button1:{width:'1.84rem'},
        button2:{width:'2.58rem'}
    }
    return (
        <div style={styles.container}>
            <div style={styles.znzLogo}><img src={znzLogo} alt="装内助logo" style={styles.logoImg}/></div>
            <div style={styles.form}>
                <div style={styles.inputCenter}>
                    <input type="text" placeholder="手机号码" onChange={this.handlegetTelVal} style={{...styles.input,...styles.bInput}}/>
                </div>
                <div style={{...styles.yzm,...styles.inputCenter}}>
                    <input type="text" placeholder="验证码" onChange={this.handlegetYZMVal} style={styles.input}/>
                    <span style={{...styles.info,...styles.info1}} onClick={this.handlegetYZM} ref="info1">获取验证码</span>
                    <span style={{...styles.info,...styles.info2}}  ref="info2">{this.state.count}秒后可以再次发送</span>
                </div>
                <button style={styles.sure} onClick={this.handleLogin}>确定</button>
                <div style={styles.btn}>
                    <button style={{...styles.button,...styles.button1}}>注册</button>
                    <button style={{...styles.button,...styles.button2}} onClick={this.handleMore}>部分功能预览</button>
                </div>
            </div>
        </div>
    );
   
  }
}
