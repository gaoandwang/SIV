import React,{ Component } from 'react';
import './setMeterial.less';
const perR    =   require('../../images/perR.png')
const left = require("../../images/left.png")
const search = require("../../images/search.png")
export default class setMeterial extends Component {
    constructor(props) {
        super(props);
        this.state={
            list:[
                { name: '木头', num: '1'},
                { name: '石头', num: '2'},
                { name: '铁皮', num: '3'},
                { name: '木头', num: '1'},
                { name: '石头', num: '2'},
                { name: '铁皮', num: '3'},
                { name: '木头', num: '1'},
                { name: '石头', num: '2'},
                { name: '铁皮', num: '3'},
                { name: '木头', num: '1'},
                { name: '石头', num: '2'},
                { name: '铁皮', num: '3'},
                { name: '木头', num: '1'},
                { name: '石头', num: '2'},
                { name: '铁皮', num: '3'},
                { name: '木头', num: '1'},
                { name: '石头', num: '2'},
                { name: '铁皮', num: '3'},
                { name: '木头', num: '1'},
                { name: '石头', num: '2'},
                { name: '铁皮', num: '3'},
                { name: '木头', num: '1'},
                { name: '石头', num: '2'},
                { name: '铁皮', num: '3'},
                { name: '木头', num: '1'},
                { name: '石头', num: '2'},
                { name: '铁皮', num: '3'},
                { name: '木头', num: '1'},
                { name: '石头', num: '2'},
                { name: '铁皮', num: '3'},
                { name: '木头', num: '1'},
                { name: '石头', num: '2'},
                { name: '铁皮', num: '3'},
            ]
        }
    }
    handleReturn(){
        window.history.go(-1);
    }
    addMeterial(){
        window.location.hash="/addMeterial"
    }
    render() {
        return (
            <div className="container">
                <div className="top">
                    <div className="img" onClick={this.handleReturn}><img src={left} style={{height:'0.32rem',width:'0.16rem'}}/></div>
                    <div className="title">设置建材库</div>
                    <div className="function">
                        <span>说明</span>
                        <span>编辑</span>
                    </div>
                </div> 
                <div className="search">
                    <img src={search} alt="查找"  style={{width:'0.36rem',height:'0.36rem'}}/>
                    <input type="text" placeholder="输入建材名称搜索"/>
                </div>
                <div className="list">
                    {this.state.list.map((item) => {
                        return (
                            <div className="items">
                                <div className="item">{item.name}</div>
                                <div><img src={perR} alt="" className="iconR"/></div>
                            </div>
                        )
                    })}
                </div>
                <div className="footer" onClick={this.addMeterial}>
                    添加建材类型
                </div>
            </div>
        );
    }
}
